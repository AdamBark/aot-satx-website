---
title: "AoT SATX 41: Titan and Luna"
date: 2024-02-26
image: post/2024-03-06-aot-satx-41/2024-03-06-AoT-SATX-41.jpg
---

The 41 Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-03-06**; talks start at 7pm, doors at 6.30pm.

Dr. Xinting Yu presents “A Flight Over the Mysterious Hydrocarbon Lakes on Saturns's Moon Titan”

Robert Reeves presents “Lunar Orbiter Divides the Old Moon from the New”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/373720542098187/) and [YouTube](https://youtube.com/live/ez1axmJwXGI)
<!--more-->
![Event Flyer](2024-03-06-AoT-SATX-41.jpg)
