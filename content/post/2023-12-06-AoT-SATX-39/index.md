---
title: "AoT SATX 39: Titanic Mysteries"
date: 2023-12-05
image: post/2023-12-06-aot-satx-39/2023-12-06-AoT-SATX-39.jpg
---

The 39th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2023-12-06**; talks start at 7pm, doors at 6.30pm.

Dr. Xinting Yu presents “A Flight Over the Mysterious Hydrocarbon Lakes on Saturn's Moon Titan”

Dr. Adolfo Santa Fe Dueñas presents “Sherlock Holmes and the Mystery of the Phase Changing and Stretching Phenomenon”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/775519721285053/) and [YouTube](https://youtube.com/live/5Os7Uv611ik)
<!--more-->
![Event Flyer](2023-12-06-AoT-SATX-39.jpg)
