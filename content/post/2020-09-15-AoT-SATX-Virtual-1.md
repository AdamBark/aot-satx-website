---
title: "AoT SATX Virtual 1"
subtitle: "Presented with Scobee Education Center"
date: 2020-09-15
tags: [virtual]
---

Astronomy on Tap SATX returns - virtually! In collaboration with Scobee Education Center, tune in next Wednesday, September 23rd, to hear our own Dr. Vincent Hue talk about the Juno Mission to Jupiter from 7pm-9pm.

![Event Flyer](/img/Virtual-1-flyer.jpg)

[Register in advance here](https://alamo.zoom.us/webinar/register/WN_Vwv8WVRKTOWKPHsdVHy8oQ)
