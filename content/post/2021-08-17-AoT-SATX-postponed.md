---
title: "AoT SATX Postponement"
date: 2021-08-17
images: /img/2021-08_postponement.jpg
---

Due to the recent surge in Bexar County Covid-19 cases we are postponing the return of in-person events, hopefully, to later this fall.
![](/img/2021-08_postponement.jpg)