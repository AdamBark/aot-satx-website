---
title: "AoT SATX online 10"
subtitle: "Presented with Scobee Education Center"
date: 2021-06-30
tags: [virtual]
---


Tonight! Tune in as we join Scobee Education Center for another virtual event.

"The Origin of the Milky Way and its Elements" by
Dr Keith Hawkins

"Exploring Dusty Planetary Surfaces from the Ground, the Sky, and Space!" by
Dr. Addie Dove.

Watch live with the Scobee Education Center on [Facebook](https://fb.me/e/HYOfE4jx) or [YouTube](https://www.youtube.com/channel/UClPBFzSD_Bes_7DloB7NM4g/featured)

Catch up anytime on with us on [YouTube](https://youtu.be/Qa6aC3n1JKg) or [facebook](https://www.facebook.com/events/508238857085612/)

![Event Flyer](/img/AoT-online-10.jpg)
