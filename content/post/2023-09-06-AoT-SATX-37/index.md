---
title: "AoT SATX 37: Solar Eclipses and Voyager"
date: 2023-09-03
image: post/2023-09-06-aot-satx-37/2023-09-06-AoT-SATX-37.jpg
---

The 37th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2023-09-06**; talks start at 7pm, doors at 6.30pm.

Rick Varner presents “SATX Solar Eclipses: Don't be Blinded by the Light”

Dr. William Kosmann presents “Voyager's Family Portrait of the Solar System”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/268984929318603/) and [YouTube](https://youtube.com/live/SflWtDLRmDk)
<!--more-->
![Event Flyer](2023-09-06-AoT-SATX-37.jpg)
