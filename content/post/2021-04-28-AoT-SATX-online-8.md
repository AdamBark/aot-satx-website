---
title: "AoT SATX online 8"
subtitle: "Presented with Scobee Education Center"
date: 2021-04-28
tags: [virtual]
---


Tonight! Tune in as we join Scobee Education Center for another virtual event.

Presenter: Frederic Allegrini
Title: The nightlife of a space nerd
Bio: Frederic grew up in Switzerland and got his astro bug… well… when he was a small kid. He was fascinated by rockets and had the dream of building a big one. Today, he is not quite building rockets, but has built or has contributed to a bunch of scientific instruments that are flying into space. His father, a photographer, instilled curiosity by giving him a camera as a present at a young age. Later, Frederic took pleasure in exploring the night skies with his telescope. All of these interests and passions merged into night- and astro-photography. He is fascinated by shapes, textures, and colors. He recently started focusing on all the wonderful things we have on Earth and in the sky, and put them in relation.

Presenter: Dr. Rachael Filwett
Title: Cloudy with a Chance of Protons: Space Weather and Your Life
Bio: Dr. Rachael Filwett received her Ph.D. in Physics from the University of Texas at San Antonio, in a joint program with the Southwest Research Institute in 2018. Since then, she has been a postdoctoral researcher at the University of Iowa working on NASA’s Van Allen Probes and MMS missions. In 2020 she received a National Science Foundation Postdoctoral Research Fellowship award, and in 2021 she was given the University of Iowa’s Postdoctoral Research Scholar Excellence Award

Watch our livestream on [Youtube](https://youtu.be/E3Pe_6xU7Sg), [Facebook](https://www.facebook.com/events/212034410385068/)

![Event Flyer](/img/AoT-online-8.jpg)
