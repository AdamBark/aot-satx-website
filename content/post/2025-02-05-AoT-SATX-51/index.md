---
title: "AoT SATX 51: Amateurs & Exoskeletons"
date: 2025-02-03
image: post/2025-02-05-aot-satx-51/2025-02-05-AoT-SATX-51.jpg
---

The 51st Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2025-02-05**; talks start at 7pm, doors at 6.30pm.

Mark Jurena presents “What do Amateur Astronomers do? Besides outreach”

Dr. W. Lee Childers presents “Exoskeletons in Space!”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/1248211372908421/) and [YouTube](https://www.youtube.com/watch?v=UCJHTtq2gzc)
<!--more-->
![Event Flyer](2025-02-05-AoT-SATX-51.jpg)
