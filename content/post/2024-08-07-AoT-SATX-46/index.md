---
title: "AoT SATX 46: Solar Sails & Cosmic Rays"
date: 2024-08-05
image: post/2024-08-07-aot-satx-46/2024-08-07-AoT-SATX-46.jpg
---

The 46th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-08-07**; talks start at 7pm, doors at 6.30pm.

Dr. Dhirendra Kataria presents “The answer my friends, is blowing in the wind - the story of solar sailing and a few other tales”

Samuel Hart presents “Discovering Cosmic Radiation - Priests, Balloons, & Solar Eclipses”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/367106813102053/) and [YouTube](https://youtube.com/live/o6oNcffjKaY)
<!--more-->
![Event Flyer](2024-08-07-AoT-SATX-46.jpg)
