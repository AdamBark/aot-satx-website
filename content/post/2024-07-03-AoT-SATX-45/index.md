---
title: "AoT SATX 45: Europa Special"
date: 2024-07-01
image: post/2024-07-03-aot-satx-45/2024-07-03-AoT-SATX-45.jpg
---

The 45th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-07-03**; talks start at 7pm, doors at 6.30pm.

Dr. Kelly Miller presents “How is NASA's Europa Clipper Mission Like a Seabird”

Dr. Ujjwal Raut presents “Exploring Europa with Spacecrafts that Never Launch”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/823317552788838/) and [YouTube](https://www.youtube.com/watch?v=8roTIdTQe7M)
<!--more-->
![Event Flyer](2024-07-03-AoT-SATX-45.jpg)
