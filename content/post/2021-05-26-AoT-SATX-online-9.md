---
title: "AoT SATX online 9"
subtitle: "Presented with Scobee Education Center"
date: 2021-05-26
tags: [virtual]
---


Tonight! Tune in as we join Scobee Education Center for another virtual event.

Title: Global Warming Demystified — The Astronomy Connection

Bio: Jeffrey Bennett holds a B.A. in Biophysics from the University of California at San Diego and an M.S. and Ph.D. in Astrophysics from the University of Colorado at Boulder. He specializes in mathematics and science education, writing for and speaking to audiences ranging from elementary school children to college faculty. His current projects include the [Story Time From Space program, which features his six children’s books being read by astronauts from the International Space Station](https://www.bigkidscience.com/), the free app "Totality by Big Kid Science" that will help you get ready for the upcoming solar eclipses in 2023 and 2024, building scale model solar systems through the [Voyage National Program](http://voyagesolarsystem.org/), and a free online textbook for middle school [Earth & Space Science](https://grade8science.com/). May 26th's program on global warming builds on his book [A Global Warming Primer](http://voyagesolarsystem.org/)

Watch our livestream on [Youtube](https://youtu.be/-XQfa6tq9rE), [Facebook](https://www.facebook.com/events/1647851672092840/)

![Event Flyer](/img/AoT-online-9.jpg)
