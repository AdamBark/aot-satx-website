---
title: "AoT SATX 49: Cataclysmic Variables & Extreme Environments"
date: 2024-11-04
image: post/2024-11-06-aot-satx-49/2024-11-06-AoT-SATX-49.jpg
---

The 49th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-11-06**; talks start at 7pm, doors at 6.30pm.

Tyler Rucas presents “A Look at Cataclysmic Variables with TESS”

Dr. Rachel Slank presents “The Dance of Desert Salts: How the Atacama's Salt Cycles Teach Us About Extreme Environments”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/2890949281071555/) and [YouTube](https://youtube.com/live/R9ezdQUoP4k)
<!--more-->
![Event Flyer](2024-11-06-AoT-SATX-49.jpg)
