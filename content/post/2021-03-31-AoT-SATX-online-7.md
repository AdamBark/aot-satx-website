---
title: "AoT SATX online 7"
subtitle: "Presented with Scobee Education Center"
date: 2021-03-31
tags: [virtual]
---


Tonight! Tune in as we join Scobee Education Center for another virtual event. Elizabeth Czajka will present her talk "**Orange is the New Black: Volcanic glass on the Moon**" and your Astronomy on Tap team will briefly cover Astronomy in the news.

Watch our livestream on [Youtube](https://youtu.be/ysxr32tIp5M), [Facebook](https://fb.me/e/2glW5P4OX) or [register to join us live on Zoom](https://alamo.zoom.us/webinar/register/WN_kRWY_-B2SUWCfNYkn_5E2A)

![Event Flyer](/img/AoT-online-7.jpg)
