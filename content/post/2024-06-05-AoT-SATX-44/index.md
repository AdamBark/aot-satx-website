---
title: "AoT SATX 44: Weather on Venus"
date: 2024-06-04
image: post/2024-06-05-aot-satx-44/2024-06-05-AoT-SATX-44.jpg
---

The 44 Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-06-05**; talks start at 7pm, doors at 6.30pm.

Dr. Sara Port presents “Snow on Venus? Exploring the Origin of the Planet's Reflective Mountains”

Dinesh Radhakrishnan presents “The Curious Case of Lightning on Venus”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/760852349585305/) and [YouTube](https://youtube.com/live/EldDrcmb8TU)
<!--more-->
![Event Flyer](2024-06-05-AoT-SATX-44.jpg)
