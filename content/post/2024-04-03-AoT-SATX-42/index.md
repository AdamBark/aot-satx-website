---
title: "AoT SATX 42: Total Solar Eclipse!"
date: 2024-04-01
image: post/2024-04-03-aot-satx-42/2024-04-03-AoT-SATX-42.jpg
---

The 42nd Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-04-03**; talks start at 7pm, doors at 6.30pm.

Dr. Angela Speck presents “The Moon's Shadow: how solar eclipses took over my life”

Cody Cly presents “Understanding Indigenous Teachings of the Eclipse”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/1894262271004882/) and [YouTube](https://youtube.com/live/uXZT6800OPE)
<!--more-->
![Event Flyer](2024-04-03-AoT-SATX-42.jpg)
