---
title: "AoT SATX 36: Building Habitable Planets; Tiny Stars"
date: 2023-07-27
image: /img/2023-08-02-flyer-AoT-36.jpg
---

The 36th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2023-08-02**; talks start at 7pm, doors at 6.30pm.

Dr. Ngoc Truong presents “How to Build a Habitable Planet for *The Little Prince*”

Sean Dillon presents “Stars the Size of San Antonio”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/289157130263084/) and [YouTube](https://youtube.com/live/Qd3Ea2pG9ag)
<!--more-->
![Event Flyer](/img/2023-08-02-flyer-AoT-36.jpg)
