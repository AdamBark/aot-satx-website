---
title: "AoT SATX 43: Exoplanets & Anime"
date: 2024-04-29
image: post/2024-05-01-aot-satx-43/2024-05-01-AoT-SATX-43.jpg
---

The 43 Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-05-01**; talks start at 7pm, doors at 6.30pm.

Dr. Amílcar Torres-Quijano presents “Using Machine Learning to Predict Host Stars and the Key Elemental Abundances of Small Planets”

Dr. Roman Gomez presents “The Physics of Anime - Abridged”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/1437474790378635/) and [YouTube](https://youtube.com/live/Pzir7Ns-u9w)
<!--more-->
![Event Flyer](2024-05-01-AoT-SATX-43.jpg)
