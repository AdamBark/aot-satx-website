---
title: "AoT SATX 52: Uranus & Planetary Defense"
date: 2025-03-01
image: post/2025-03-05-aot-satx-52/2025-03-05-AoT-SATX-52.jpg
---

The 52nd Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2025-03-05**; talks start at 7pm, doors at 6.30pm.

Dr. Maryame El Moutamid presents “Why We Should Explore the Planet Uranus?”

Aaron Deleon presents “Our First and Last Lines of Planetary Defense”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/989350893138727/) and [YouTube](https://youtube.com/live/pnqrbaj1zM0)
<!--more-->
![Event Flyer](2025-03-05-AoT-SATX-52.jpg)
