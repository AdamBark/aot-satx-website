---
title: "AoT SATX 40: Solar Eclipse & Cosmic Giants"
date: 2024-02-05
image: post/2024-02-07-aot-satx-40/2024-02-07-AoT-SATX-40.jpg
---

The 40th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-02-07**; talks start at 7pm, doors at 6.30pm.

Dr. Lindsay Fuller presents “Total Eclipse of the Heart of Texas”

Dr. Dana Koeppe presents “Cosmic Giants”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/3055104911286864/) and [YouTube](https://youtube.com/live/EKQsJEUA2kE)
<!--more-->
![Event Flyer](2024-02-07-AoT-SATX-40.jpg)
