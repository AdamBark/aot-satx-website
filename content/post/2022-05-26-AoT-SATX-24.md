---
title: "AoT SATX 24: Imaging Black Holes"
date: 2022-05-27
image: /img/2022-06-01-flyer-AoT-24.jpg
---

The 24th Astronomy on Tap at the Blue Star Brewery in San Antonio, Texas will occur on **1st June 2022** talks start at 7pm, doors at 6.30pm.

Dr. Richard Anantua from UTSA will be telling us about his role in imaging the black hole at the center of our galaxy, and Jackie Champagne from UT Austin will be teaching us about radio interferometry, the technique that was used to acquire it!

If you can't make it in person watch live or after the event on [YouTube](https://youtu.be/87z1eSEp6GY) or [facebook](https://www.facebook.com/events/1172508946935431)

![Event Flyer](/img/2022-06-01-flyer-AoT-24.jpg)
