---
title: "AoT SATX 50: Celebrating 50 Events!"
date: 2024-12-02
image: post/2024-12-04-aot-satx-50/2024-12-04-AoT-SATX-50.jpg
---

The 50th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-12-04**; talks start at 7pm, doors at 6.30pm.

Dr. Jim Burch presents “Seeing the Invisible in Space: Then and Now”

Dr. Mona El Morsy presents “Unveiling Exoplanets: Characterizing Them with Advanced Techniques”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/607346575275028/) and [YouTube](https://youtube.com/live/SYKMzc9wVIQ)
<!--more-->
![Event Flyer](2024-12-04-AoT-SATX-50.jpg)
