---
title: "AoT SATX 38: Outreach & JWST"
date: 2023-10-31
image: post/2023-11-01-aot-satx-38/2023-11-01-AoT-SATX-38.jpg
---

The 38th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2023-11-01**; talks start at 7pm, doors at 6.30pm.

Dan Cruz presents “The Importance of Outreach in Astronomy”

Mason Leist presents “JWST and the Infrared Universe: From Point Sources to Galaxies”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/298126926486821/) and [YouTube](https://www.youtube.com/watch?v=yh0BsLpatqU)
<!--more-->
![Event Flyer](2023-11-01-AoT-SATX-38.jpg)
