---
title: "AoT SATX online 11"
subtitle: "Presented with Scobee Education Center"
date: 2021-07-21
publishDate: 2021-07-15
images: /img/AoT-online-11.jpg
tags: [virtual]
---

# Unfold the Unknown with the James Webb Space Telescope

Wednesday 21st July 19:00 CDT! Tune in as we join Scobee Education Center for another virtual event. Come and learn a thing or two about the soon to launch James Webb Space Telescope from Dr. Alexandra Lockwood!
![Event Flyer](/img/AoT-online-11.jpg)
<!--more-->

###### Abstract ######

Learn about the ambitious science and incredible engineering of NASA’s next great observatory – the James Webb Space Telescope! The Webb Space Telescope is a very large infrared telescope, often referred to as ‘the successor to Hubble’. It is launching THIS November, and there is so much to learn about it and from it! Webb will be used to explore fundamental questions in astronomy, including what the first galaxies were like and what is the atmospheres of planets near and far. Come learn more about this legendary mission.

###### Biography ######

 Alex is the Project Scientist for Webb Science Communications at the Space Telescope Science Institute. She received her BSin Physics and Astronomy from the University of Maryland and a MS and PhD from Caltech in Planetary Sciences. During graduate school she also starred in The PhD Movie, a live action film based off the popular webcomic www.phdcomics.com. Previously she worked on the NOAA/NASA Joint Polar Satellite System mission and at the King Abdullah University of Science and Technology in Saudi Arabia, both in communications and outreach roles. Outside of work she loves running, yoga, and laughing with her daughter.

Watch live with the Scobee Education Center on [facebook](https://www.facebook.com/events/801165187248270/) or [YouTube](https://www.youtube.com/channel/UClPBFzSD_Bes_7DloB7NM4g/featured)

Catch up anytime on with us on [YouTube](https://www.youtube.com/watch?v=zjOI1xes868) or [facebook](https://www.facebook.com/events/133132522283223/)
