---
title: "AoT SATX 47: Space Weather Report"
date: 2024-08-30
image: post/2024-09-04-aot-satx-47/2024-09-04-AoT-SATX-47.jpg
---

The 47th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-09-04**; talks start at 7pm, doors at 6.30pm.

Dr. Robert Ebert presents “Making SWiPS: The Solar Wind Monitor for NOAA's Space Weather Mission, SWFO-L1”

Kimberly Moreland presents “Unlocking the Secrets of the Sun - Inside NOAA's Space Weather Prediction Center”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/1054315032935829/) and [YouTube](https://www.youtube.com/watch?v=jLIIzyYJbMw)
<!--more-->
![Event Flyer](2024-09-04-AoT-SATX-47.jpg)
