---
title: "AoT SATX 48: Establishing Kosovo's First National Observatory"
date: 2024-09-27
image: post/2024-10-02-aot-satx-48/2024-10-02-AoT-SATX-48.jpg
---

The 48th Astronomy on Tap at the [Blue Star Brewery](https://bluestarbrewing.com) in San Antonio, Texas will occur on **Wednesday 2024-10-02**; talks start at 7pm, doors at 6.30pm.

Pranvera Hyseni presents “Establishing Kosovo's First National Observatory”

Robert Reeves also presents “Establishing Kosovo's First National Observatory”

The event will also be available to stream live or afterwards on [facebook](https://www.facebook.com/events/897316882290110/) and [YouTube](https://youtube.com/live/hBdMTqW2wzE)
<!--more-->
![Event Flyer](2024-10-02-AoT-SATX-48.jpg)
