---
title: "AoT SATX online 6"
subtitle: "Presented with Scobee Education Center"
date: 2021-02-24
tags: [virtual]
---

Tonight! Tune in as we join Scobee Education Center for another virtual event. Dr. Stephanie Jarmak will present “Space Dirt in Zero-G” about experiments in microgravity to better understand collisions in space. Dr. Tracy Becker will present “The Legacy and Future of the Arecibo Observatory” about the world’s once-largest single dish radio telescope that collapsed in December 2020 and what the plans are for its future.
Watch our livestream on [Facebook](https://www.facebook.com/AoT.SATX/posts/1413569102323781), [YouTube](https://youtu.be/NxygHk27MkY) or [register to join us live on Zoom](https://alamo.zoom.us/webinar/register/WN_AakWjiC0TsSoHTZdQ-yx4Q)

![Event Flyer](/img/AoT-online-6.jpg)
