## Welcome

If you like beer and hearing astronomers talk about astronomy you've come to the right place. Actually the right place is [Blue Star Brewing Company](http://bluestarbrewing.com/) but you get the idea. Follow us on Facebook or [subscribe to the RSS feed](/index.xml) to keep up-to-date with our events or watch old ones on our YouTube channel.

If that's not enough Astronomy and beer for you [check out our parent organisation to find your nearest Astronomy on Tap event.](https://astronomyontap.org/)
